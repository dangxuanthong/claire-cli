import {AbstractSocketController, ISocket, Message} from "claire-framework";
import {ChatChannel} from "../api/ChatChannel";
import Join = ChatChannel.Join;

export class ChatController extends AbstractSocketController {

    public constructor() {
        super();
    }

    @Message(Join.channel, Join.message)
    public async join(socket: ISocket, data: any) {
        console.log(data)
    }

}
