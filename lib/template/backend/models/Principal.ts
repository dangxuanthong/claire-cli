import {Charset, DataTable, Default, PrimaryKey} from "claire-framework";

@DataTable(Charset.UTF8)
export class Principal {

    @PrimaryKey(true)
    public id: number;

    @Default(false)
    public disabled: boolean;

    public static id = "id";
    public static disabled = "disabled";

}
