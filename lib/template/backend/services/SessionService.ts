import {AbstractService, IAppContext} from "claire-framework";
import {Crypto} from "../utils/Crypto";
import {EnvSheet} from "../env/EnvSheet";
import {Session} from "../models/Session";
import {Principal} from "../models/Principal";

export class SessionService extends AbstractService {

    private envProvider: EnvSheet;

    public constructor() {
        super();
    }

    public async init(appContext: IAppContext): Promise<void> {
        this.envProvider = appContext.getEnvProvider().load(EnvSheet);
    }

    public async createSession(principal: Principal, sessionExpirationDuration: string): Promise<Session> {
        let token = await Crypto.generateJsonToken(this.envProvider.JWT_KEY!, {...principal}, sessionExpirationDuration);
        return {
            principal_id: principal.id,
            token
        };
    }

}
