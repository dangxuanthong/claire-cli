import {ClaireError, IAppContext} from "claire-framework";
import {claireBuilder} from "./app";
import {EnvSheet} from "./env/EnvSheet";

claireBuilder.build().start()
    .then((appContext: IAppContext) => {
        appContext.getLogger().info("Server is listening at port", appContext.getEnvProvider().load(EnvSheet).PORT);
    })
    .catch((err: ClaireError) => {
        console.log(err.name, err.message);
        process.exit();
    });
