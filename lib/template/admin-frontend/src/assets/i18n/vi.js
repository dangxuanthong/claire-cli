module.exports = {
    "remember_me": "Ghi nhớ tài khoản",
    "forgot_password": "Quên mật khẩu",
    "log_in": "Đăng nhập",
    "username": "Tên đăng nhập",
    "password": "Mật khẩu",
};
