import {AbstractHttpMiddleware, HttpConnection} from "claire-framework";

export class CORS extends AbstractHttpMiddleware {

    public constructor() {
        super();
    }

    intercept() {
        return (connection: HttpConnection, next: (err?: any) => void): void => {
            connection.response.header("Access-Control-Allow-Methods", "OPTIONS, GET, POST, PUT, DELETE");
            connection.response.header("Access-Control-Allow-Origin", "*");
            connection.response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
            return next();
        };
    }

}
