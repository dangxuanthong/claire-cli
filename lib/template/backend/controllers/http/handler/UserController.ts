import {
    AbstractAccessCondition,
    ConditionPrototype,
    ConditionValueType,
    DefaultHttpResourceController,
    IAppContext,
    IDatabaseAdapter,
    IHttpRequest,
    Mapping,
    Override,
    Permission,
    Validator
} from "claire-framework";
import {User} from "../../../models/User";
import {CreateNewUser, USER_AND_APP_MANAGEMENT} from "../api/UserAndAppManagement";
import {UserService} from "../../../services/UserService";


class FilterUserInfo extends AbstractAccessCondition<string[]> {

    private readonly fields: string[];

    public constructor(requestedValueResolver: (request: IHttpRequest, appContext: IAppContext) => string[]) {
        super(requestedValueResolver);
        this.fields = Object.keys(User);
    }

    public getConditionPrototype(): ConditionPrototype {
        return {
            operatorName: "filter_user_info",
            valueType: ConditionValueType.CHOICES,
            valueConstraint: JSON.stringify(this.fields),
        };
    };

    public async validate(requestedValue: string[], permittedValue: string[]) {
        return requestedValue.every((v) => permittedValue.includes(v));
    }
}


export class UserController extends DefaultHttpResourceController<User> {

    private userService: UserService;
    private databaseAdapter: IDatabaseAdapter;

    constructor(model: typeof User) {
        super(model);
    }

    public async init(appContext: IAppContext) {
        await super.init(appContext);
        this.databaseAdapter = appContext.getDatabaseAdapter();
        this.userService = appContext.getServiceProvider().get(UserService);
    }

    @Override()
    @Validator(CreateNewUser.request, CreateNewUser.response)
    @Permission(USER_AND_APP_MANAGEMENT, CreateNewUser.permissionType, CreateNewUser.permissionConditions)
    @Mapping(CreateNewUser.method, CreateNewUser.url)
    public async createNewUser(request: IHttpRequest): Promise<CreateNewUser.response> {

        //-- create new user
        let body = request.getBody(CreateNewUser.request);
        let result = await this.userService.createNewUser(body.email, body.password, body.first_name, body.last_name, body.phone_number);
        return {id: result.user.id, principal_id: result.principal.id};

    }

    @Permission(undefined, undefined, [
        new FilterUserInfo((request: IHttpRequest, appContext: IAppContext) => {
            //-- filter query will automatically be parsed into array thank to the default queryValidator.
            return request.getQuery()["filter"];
        })
    ])
    public async getAllResources(request: IHttpRequest) {
        return super.getAllResources(request);
    }

}

