//-- import native and external libraries
import 'source-map-support/register';
import * as path from "path";
//-- import framework
import {
    ClaireBuilder,
    ConsoleLogMedia,
    DefaultApiDocController,
    DefaultEnvProvider,
    DefaultHttpRequestHandler, DefaultHttpResourceController,
    DefaultLogger,
    DefaultSocketRequestHandler,
    DefaultSqlAdapter,
    SqlProvider,
    FileLogMedia,
    NoSqlProvider,
    DefaultNoSqlAdapter,
} from "claire-framework";
//-- import env and package.json
import * as packageJson from "./package.json";
import {EnvSheet} from "./env/EnvSheet";
//-- import models
import {Role} from "./models/Role";
import {User} from "./models/User";
import {PrincipalRole} from "./models/PrincipalRole";
import {Auth} from "./models/Auth";
import {Policy} from "./models/Policy";
import {RolePolicy} from "./models/RolePolicy";
import {PolicyCondition} from "./models/PolicyCondition";
import {PolicyPermission} from "./models/PolicyPermission";
import {App} from "./models/App";
import {Principal} from "./models/Principal";
//-- import services
import {SessionService} from "./services/SessionService";
//-- import middleware
import {CORS} from "./middleware/CORS";
import {BodyParser} from "./middleware/BodyParser";
import {SocketAuth} from "./middleware/SocketAuth";
//-- import controllers
import {SessionController} from "./controllers/http/handler/SessionController";
import {UserController} from "./controllers/http/handler/UserController";
import {ChatController} from "./controllers/socket/handler/ChatController";
//-- import bootstrapper
import {Bootstrap} from "./bootstrap";
import {RequestLogger} from "./middleware/RequestLogger";
import {HttpAuthorizationProvider} from "./controllers/http/HttpAuthorizationProvider";
import {SocketAuthorizationProvider} from "./controllers/socket/SocketAuthorizationProvider";
import {AppController} from "./controllers/http/handler/AppController";
import {UserService} from "./services/UserService";

// ======================================== BUILD AND RUN THE APP ==========================================

//-- create builder
const claireBuilder = new ClaireBuilder();

//-- create environment provider
const envProvider = new DefaultEnvProvider(path.join(__dirname, "env"), (currentEnv: string) => (currentEnv + ".env"));
const envSheet = envProvider.load(EnvSheet);

//-- create logger
const logger = new DefaultLogger(envSheet.LOG_LIMIT, [
    envProvider.getCurrentEnv() !== "PRO" ? new ConsoleLogMedia(true) : undefined,
    new FileLogMedia(path.join(__dirname, "log"), 1, true)
], "DD/MM/YYYY HH:mm:ssZ", 1000);

//-- build the claire app
claireBuilder.setEnvProvider(envProvider);
claireBuilder.setLogger(logger);

claireBuilder.setDatabaseAdapter(
    // new DefaultNoSqlAdapter(
    //     NoSqlProvider.MONGODB,
    //     envSheet.MONGO_CONNECTION_STRING!,
    // [Principal, App, User, Role, Auth, PrincipalRole, Policy, RolePolicy, PolicyCondition, PolicyPermission]
    // )
    new DefaultSqlAdapter(
        SqlProvider.MY_SQL,
        envSheet.SQL_CONNECTION_STRING!,
        path.join(__dirname, "models", "migration"),
        [Principal, App, User, Role, Auth, PrincipalRole, Policy, RolePolicy, PolicyCondition, PolicyPermission]
    )
);

claireBuilder.setServices([
    new SessionService(),
    new UserService(),
]);

claireBuilder.setBootstrapper(new Bootstrap());

claireBuilder.setHttpRequestHandler(
    new DefaultHttpRequestHandler(
        envSheet.PORT,
        "/api",
        [
            new CORS(),
            new BodyParser(),
            new RequestLogger(),
        ],
        [
            new SessionController(),
            new UserController(User),
            new AppController(App),
            new DefaultHttpResourceController(Role),
            new DefaultHttpResourceController(Policy),
            new DefaultHttpResourceController(PrincipalRole),
            new DefaultHttpResourceController(RolePolicy),
            new DefaultHttpResourceController(PolicyPermission),
            new DefaultHttpResourceController(PolicyCondition),
            envProvider.getCurrentEnv() !== "PRO" ? new DefaultApiDocController(packageJson, "/apidocs", true) : undefined,
        ],
        new HttpAuthorizationProvider()
    )
);

claireBuilder.setSocketRequestHandler(
    new DefaultSocketRequestHandler(
        envSheet.PORT,
        "/socket",
        [
            new SocketAuth(envSheet.JWT_KEY!),
        ],
        [
            new ChatController()
        ],
        new SocketAuthorizationProvider()
    )
);

export {claireBuilder};
