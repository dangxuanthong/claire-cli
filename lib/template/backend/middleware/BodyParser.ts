import bodyParser from "body-parser";
import fileUpload from "express-fileupload";
import {AbstractHttpMiddleware, HttpConnection} from "claire-framework";

export class BodyParser extends AbstractHttpMiddleware {

    public constructor() {
        super();
    }

    public intercept() {
        return (connection: HttpConnection, next: (err?: any) => void): void => {
            //-- parse json
            return bodyParser.json()(connection.request, connection.response, () => {
                //- parse uploaded files
                fileUpload({
                    limits: {
                        files: 3, //-- maximum number of files
                        fileSize: 50 * 1024 * 1024, //-- max file size in byte
                    }
                })(connection.request, connection.response, () => {
                    //-- map file to body
                    let files = connection.request.files;
                    if (files) {
                        let keys = Object.keys(files);
                        for (let i = 0; i < keys.length; i++) {
                            connection.request.body[keys[i]] = files[keys[i]];
                        }
                    }
                    next();
                });
            });
        }
    }

}
