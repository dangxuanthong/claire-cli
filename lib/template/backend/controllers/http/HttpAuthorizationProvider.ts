import {
    AbstractHttpAuthorizationProvider, belongs,
    ClaireError, eq,
    HttpRouteHandler,
    IAppContext, IDatabaseAdapter,
    IHttpRequest, ILogger
} from "claire-framework";
import {Errors} from "../../utils/Errors";
import {EnvSheet} from "../../env/EnvSheet";
import {Principal} from "../../models/Principal";
import {Crypto} from "../../utils/Crypto";
import {PrincipalRole} from "../../models/PrincipalRole";
import {RolePolicy} from "../../models/RolePolicy";
import {PolicyPermission} from "../../models/PolicyPermission";
import {PolicyCondition} from "../../models/PolicyCondition";

export class HttpAuthorizationProvider extends AbstractHttpAuthorizationProvider {

    private jwtKey: string;
    private databaseAdapter: IDatabaseAdapter;
    private appContext: IAppContext;
    private logger: ILogger;

    public constructor() {
        super();
    }

    public async init(appContext: IAppContext) {
        this.appContext = appContext;
        this.jwtKey = appContext.getEnvProvider().load(EnvSheet).JWT_KEY!;
        this.databaseAdapter = appContext.getDatabaseAdapter();
        this.logger = appContext.getLogger();
    }

    public async authorize(request: IHttpRequest, handlerMetadata: HttpRouteHandler): Promise<void> {

        let token = request.headers.authorization;
        if (!token) {
            throw new ClaireError(Errors.AUTHORIZATION_REQUIRED);
        }

        let principal: Principal;
        try {
            principal = await Crypto.parseJsonToken(this.jwtKey, token) as Principal;
        } catch (err) {
            throw new ClaireError(Errors.TOKEN_EXPIRED);
        }

        //-- TODO: use cache for faster query

        //-- get all roles and policies
        let roles = await this.databaseAdapter.use(PrincipalRole).getMany([{[PrincipalRole.principal_id]: eq(principal.id)}], {projection: [PrincipalRole.role_id]});
        let policies = await this.databaseAdapter.use(RolePolicy).getMany([{[RolePolicy.role_id]: belongs(roles.map(r => r.role_id))}], {projection: [RolePolicy.policy_id]});

        let requestedPermission = `${handlerMetadata.method}${handlerMetadata.url}`;

        //-- for each policy, find all permissions and conditions
        let permissions = await this.databaseAdapter.use(PolicyPermission).getMany([{[PolicyPermission.permission]: eq(requestedPermission)}, {[PolicyPermission.policy_id]: belongs(policies.map(p => p.policy_id))}]);

        this.logger.debug(permissions);

        if (!permissions.length) {
            throw new ClaireError(Errors.ACCESS_DENIED);
        }

        if (!handlerMetadata.permissionConditions) {
            //-- ok do not need to check because the handler have no condition
            return;
        }

        let conditions = await this.databaseAdapter.use(PolicyCondition).getMany([{[PolicyCondition.policy_id]: belongs(policies.map(p => p.policy_id))}]);

        this.logger.debug(conditions);

        if (!conditions.length) {
            //-- ok do not need to check because no condition has been set
            return;
        }

        //-- group permissions and conditions into policies
        let policyPermissions = permissions.map(per => ({
            permission: per,
            conditions: conditions
                .filter(con => con.policy_id === per.policy_id)
                .filter(con => !!handlerMetadata.permissionConditions!.find((condition) => condition.getConditionPrototype().operatorName === con.operator)),
        }));

        this.logger.debug(policyPermissions);

        //-- at least one per-con pair passes
        for (let i = 0; i < policyPermissions.length; i++) {
            let percon = policyPermissions[i];
            if (!percon.conditions.length) {
                //-- no condition for this permission
                return;
            }

            for (let j = 0; j < percon.conditions.length; j++) {
                let con = percon.conditions[j];
                let handlerCon = handlerMetadata.permissionConditions!.find((condition) => condition.getConditionPrototype().operatorName === con.operator);
                //-- handler con must have existed
                let requestedValue = await handlerCon!.requestedValueResolver(request, this.appContext);
                if (requestedValue === undefined) {
                    //-- requested value must exited
                    throw new ClaireError(Errors.ACCESS_DENIED);
                }
                if (await handlerCon!.validate(requestedValue, JSON.parse(con.value))) {
                    return;
                }
            }
        }

        //-- if not returned
        throw new ClaireError(Errors.ACCESS_DENIED);
    }

}
