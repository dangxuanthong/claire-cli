//-- multi language
import Vue from "vue";
import VueI18n from 'vue-i18n';
import en from "../assets/i18n/en";
import vi from "../assets/i18n/vi";

Vue.use(VueI18n);
const i18n = new VueI18n({
    locale: 'en',
    messages: {
        en, vi
    }
});

export {i18n};
