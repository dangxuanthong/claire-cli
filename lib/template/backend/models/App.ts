import {Charset, DataTable, ForeignKey, PrimaryKey, Unique} from "claire-framework";
import {Principal} from "./Principal";

@DataTable(Charset.UTF8)
export class App {

    @PrimaryKey(true)
    public id: number;

    @Unique()
    public app_name: string;

    @ForeignKey(Principal)
    public principal_id: number;

    public static id = "id";
    public static app_name = "app_name";
    public static principal_id = "principal_id";
}
