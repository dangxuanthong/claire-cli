import {EnvTemplate, EnvVar, LogLimit} from "claire-framework";

@EnvTemplate()
export class EnvSheet {
    @EnvVar() PORT: number;
    @EnvVar() LOG_LIMIT: LogLimit;
    @EnvVar() JWT_DURATION: string;

    @EnvVar() SQL_CONNECTION_STRING?: string;
    @EnvVar() MONGO_CONNECTION_STRING?: string;
    @EnvVar() JWT_KEY?: string;
    @EnvVar() ADMIN_EMAIL?: string;
    @EnvVar() INITIAL_ADMIN_PASSWORD?: string;
}
