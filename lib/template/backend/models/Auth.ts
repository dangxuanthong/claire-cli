import {Charset, DataField, DataTable, DataType, ForeignKey, Nullable, PrimaryKey} from "claire-framework";
import {Principal} from "./Principal";

export enum AuthType {
    PASSWORD,
    API_KEY,
}

@DataTable(Charset.UTF8)
export class Auth {

    @PrimaryKey(true)
    public id: number;

    @ForeignKey(Principal)
    public principal_id: number;

    @DataField(DataType.INTEGER)
    public auth_type: AuthType;

    @Nullable()
    public auth_data1?: string;

    @Nullable()
    public auth_data2?: string;

    public static id = "id";
    public static principal_id = "principal_id";
    public static auth_type = "auth_type";
    public static auth_data1 = "auth_data1";
    public static auth_data2 = "auth_data2";

}
