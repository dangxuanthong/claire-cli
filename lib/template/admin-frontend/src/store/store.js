import Vue from "vue";
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

import {
    LOG_IN,
    LOG_OUT,

    CREATE_POLICY,
    GET_ALL_POLICIES,
    UPDATE_POLICY,
    DELETE_POLICY,

    CREATE_ROLE,
    GET_ALL_ROLES,
    UPDATE_ROLE,
    DELETE_ROLE,

    CREATE_USER,
    GET_ALL_USERS,
    UPDATE_USER,
    DELETE_USER,
    DISABLE_PRINCIPAL,
} from "./actions";
import {
    USER_LOGGED_IN,
    USER_LOGGED_OUT,

    MERGE_ROLES,
    REMOVE_ROLES,

    MERGE_USERS,
    REMOVE_USERS,

    MERGE_POLICIES,
    REMOVE_POLICIES,

    UPDATE_PRINCIPALS,
    UPDATE_PRINCIPAL_ROLES,
    REMOVE_PRINCIPAL_ROLES,

    UPDATE_ROLE_POLICIES,
    UPDATE_POLICY_CONDITIONS,
    SET_ALL_PERMISSIONS,
    REMOVE_ROLE_POLICIES,
    REMOVE_POLICY_PERMISSIONS,
    MERGE_POLICY_PERMISSIONS,
    MERGE_POLICY_CONDITIONS,
    REMOVE_POLICY_CONDITIONS,
} from "./mutations";

const escapeRegex = (value) => {
    return value.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
};

const configStore = () => new Vuex.Store({
    state: {
        user: undefined,
        roles: [],
        users: [],
        principalRoles: [],
        principals: [],
        policies: [],
        rolePolicies: [],
        policyConditions: [],
        policyPermissions: [],
        allPermissions: [],
    },
    mutations: {
        [USER_LOGGED_IN](state, principal) {
            state.user = principal;
        },
        [USER_LOGGED_OUT](state) {
            state.user = undefined;
            state.roles = [];
            state.users = [];
            state.principals = [];
        },

        //-- roles
        [MERGE_ROLES](state, roles) {
            //-- merge the roles with current state roles
            roles.forEach(role => {
                let foundIndex = state.roles.findIndex((r) => r.id === role.id);
                if (foundIndex < 0) {
                    state.roles.push(role);
                } else {
                    state.roles[foundIndex] = Object.assign(state.roles[foundIndex], role);
                }
            });
        },
        [REMOVE_ROLES](state, roles) {
            roles.forEach(role => {
                let roleIndex = state.roles.findIndex(r => r.id === role.id);
                if (roleIndex >= 0) {
                    state.roles.splice(roleIndex, 1);
                }
            });
        },

        //-- users
        [MERGE_USERS](state, users) {
            users.forEach(user => {
                let foundIndex = state.users.findIndex((u) => u.id === user.id);
                if (foundIndex < 0) {
                    state.users.push(user);
                } else {
                    state.users[foundIndex] = Object.assign(state.users[foundIndex], user);
                }
            });
        },
        [REMOVE_USERS](state, users) {
            if (!users) {
                state.users = [];
            } else {
                users.forEach(user => {
                    let index = state.users.findIndex(u => u.id === user.id);
                    if (index >= 0) {
                        state.users.splice(index, 1);
                    }
                });
            }
        },

        [MERGE_POLICIES](state, policies) {
            //-- merge the policies with current state policies
            policies.forEach(policy => {
                let foundIndex = state.policies.findIndex((p) => p.id === policy.id);
                if (foundIndex < 0) {
                    state.policies.push(policy);
                } else {
                    state.policies[foundIndex] = Object.assign(state.policies[foundIndex], policy);
                }
            });
        },
        [REMOVE_POLICIES](state, policies) {
            //-- remove the policy, corresponding permissions and conditions
            policies.forEach(policy => {
                let foundIndex = state.policies.findIndex((p) => p.id === policy.id);
                if (foundIndex >= 0) {
                    state.policies.splice(foundIndex, 1);
                }
                for (let i = state.policyPermissions.length - 1; i >= 0; i--) {
                    if (state.policyPermissions[i].policy_id === policy.id) {
                        state.policyPermissions.splice(i, 1);
                    }
                }
                for (let i = state.policyConditions.length - 1; i >= 0; i--) {
                    if (state.policyConditions[i].policy_id === policy.id) {
                        state.policyConditions.splice(i, 1);
                    }
                }
            });
        },

        [UPDATE_ROLE_POLICIES](state, rolePolicies) {
            rolePolicies.forEach(rolePolicy => {
                let index = state.rolePolicies.findIndex(rp => rp.id === rolePolicy.id);
                if (index < 0) {
                    state.rolePolicies.push(rolePolicy);
                } else {
                    Object.assign(state.rolePolicies[index], rolePolicy);
                }
            });
        },
        [UPDATE_PRINCIPALS](state, principals) {
            state.principals = principals;
        },
        [DISABLE_PRINCIPAL](state, principal) {
            let updatedPrincipal = state.principals.find(p => p.id === principal.id);
            if (updatedPrincipal) {
                updatedPrincipal.disabled = principal.disabled;
            }
        },

        [UPDATE_PRINCIPAL_ROLES](state, principalRoles) {
            principalRoles.forEach(pr => {
                let index = state.principalRoles.findIndex(p => p.id === pr.id);
                if (index < 0) {
                    state.principalRoles.push(pr);
                } else {
                    Object.assign(state.principalRoles[index], pr);
                }
            });
        },
        [UPDATE_POLICY_CONDITIONS](state, conditions) {
            conditions.forEach(condition => {
                let index = state.policyConditions.findIndex(c => c.id === condition.id);
                if (index < 0) {
                    state.policyConditions.push(condition);
                } else {
                    Object.assign(state.policyConditions[index], condition);
                }
            });
        },
        [REMOVE_PRINCIPAL_ROLES](state, principalId) {
            for (let i = state.principalRoles.length - 1; i >= 0; i--) {
                if (state.principalRoles[i].principal_id === principalId) {
                    state.principalRoles.splice(i, 1);
                }
            }
        },
        [REMOVE_ROLE_POLICIES](state, roleId) {
            for (let i = state.rolePolicies.length - 1; i >= 0; i--) {
                if (state.rolePolicies[i].role_id === roleId) {
                    state.rolePolicies.splice(i, 1);
                }
            }
        },

        [MERGE_POLICY_PERMISSIONS](state, permissions) {
            permissions.forEach(permission => {
                let index = state.policyPermissions.findIndex(p => p.id === permission.id);
                if (index < 0) {
                    state.policyPermissions.push(permission);
                } else {
                    Object.assign(state.policyPermissions[index], permission);
                }
            });
        },
        [MERGE_POLICY_CONDITIONS](state, conditions) {
            conditions.forEach(con => {
                let index = state.policyConditions.findIndex(p => p.id === con.id);
                if (index < 0) {
                    state.policyConditions.push(con);
                } else {
                    Object.assign(state.policyConditions[index], con);
                }
            });
        },
        [REMOVE_POLICY_PERMISSIONS](state, policyId) {
            for (let i = state.policyPermissions.length - 1; i >= 0; i--) {
                if (state.policyPermissions[i].policy_id === policyId) {
                    state.policyPermissions.splice(i, 1);
                }
            }
        },
        [REMOVE_POLICY_CONDITIONS](state, policyId) {
            for (let i = state.policyConditions.length - 1; i >= 0; i--) {
                if (state.policyConditions[i].policy_id === policyId) {
                    state.policyConditions.splice(i, 1);
                }
            }
        },
        [SET_ALL_PERMISSIONS](state, allPermissions) {
            state.allPermissions = allPermissions;
        },
    },
    actions: {
        [LOG_IN]({commit}, {username, password}) {
            return new Promise((resolve, reject) => {
                (async () => {
                    let response = await axios.post(process.env.VUE_APP_API_SERVER_ENDPOINT + "/session", {
                        type: 0,
                        authData1: username,
                        authData2: password,
                    });
                    //eslint-disable-next-line
                    console.log(response.data);
                    if (!response.data.success) {
                        return reject(response.data.result);
                    }

                    let principal = response.data.result;
                    //-- fetch user info
                    response = await axios.get(process.env.VUE_APP_API_SERVER_ENDPOINT + "/user?principal_id=" + JSON.stringify([response.data.result.principal_id]), {
                        headers: {"Authorization": response.data.result.token}
                    });
                    if (!response.data.success) {
                        return reject(response.data.result);
                    }
                    commit(USER_LOGGED_IN, {principal, userInfo: response.data.result[0]});
                    return resolve();
                })();
            });
        },
        [LOG_OUT]({commit}) {
            return new Promise((resolve) => {
                commit(USER_LOGGED_OUT);
                resolve();
            });
        },

        //-- roles
        [CREATE_ROLE]({commit, state}, {role_name, is_system_role}) {
            return new Promise((resolve, reject) => {
                (async () => {
                    let response = await axios.post(process.env.VUE_APP_API_SERVER_ENDPOINT + "/role",
                        {
                            is_system_role,
                            role_name,
                        }, {headers: {"Authorization": state.user.principal.token}});

                    if (!response.data.success) {
                        return reject(response.data.result);
                    }
                    commit(MERGE_ROLES, [{id: response.data.result.id, role_name, is_system_role}]);
                    return resolve(response.data.result.id);
                })();
            });
        },
        [GET_ALL_ROLES]({commit, state}) {
            return new Promise((resolve, reject) => {
                (async () => {

                    let roles = await axios.get(process.env.VUE_APP_API_SERVER_ENDPOINT + "/role",
                        {headers: {"Authorization": state.user.principal.token}});

                    if (!roles.data.success) {
                        return reject(roles.data.result);
                    }

                    let rolePolicies = await axios.get(process.env.VUE_APP_API_SERVER_ENDPOINT + "/rolepolicy",
                        {headers: {"Authorization": state.user.principal.token}});
                    if (!rolePolicies.data.success) {
                        return reject(rolePolicies.data.success);
                    }

                    commit(MERGE_ROLES, roles.data.result);
                    commit(UPDATE_ROLE_POLICIES, rolePolicies.data.result);
                    return resolve();
                })();
            });
        },
        [UPDATE_ROLE]({commit, state}, {id, role_name, is_system_role, policy_id}) {
            return new Promise((resolve, reject) => {
                (async () => {
                    let roleResult = await axios.put(process.env.VUE_APP_API_SERVER_ENDPOINT + "/role/" + id,
                        {
                            role_name, is_system_role,
                        }, {headers: {"Authorization": state.user.principal.token}});

                    if (!roleResult.data.success) {
                        return reject(roleResult.data.result);
                    }

                    let policyResult = await axios.put(process.env.VUE_APP_API_SERVER_ENDPOINT + "/role/" + id + "/policy",
                        {
                            policy_id
                        }, {headers: {"Authorization": state.user.principal.token}});

                    if (!policyResult.data.success) {
                        return reject(policyResult.data.result);
                    }

                    commit(MERGE_ROLES, [{id, role_name, is_system_role}]);
                    commit(REMOVE_ROLE_POLICIES, id);
                    commit(UPDATE_ROLE_POLICIES, policyResult.data.result);
                    return resolve();
                })();
            });
        },
        [DELETE_ROLE]({commit, state}, {id}) {
            return new Promise((resolve, reject) => {
                (async () => {
                    let response = await axios.delete(process.env.VUE_APP_API_SERVER_ENDPOINT + "/role/" + id,
                        {headers: {"Authorization": state.user.principal.token}});

                    if (!response.data.success) {
                        return reject(response.data.result);
                    }
                    commit(REMOVE_ROLES, [{id}]);
                    return resolve();
                })();
            });
        },

        //-- users
        [CREATE_USER]({state, commit}, {email, first_name, last_name, phone_number}) {
            return new Promise((resolve, reject) => {
                (async () => {
                    let response = await axios.post(process.env.VUE_APP_API_SERVER_ENDPOINT + "/user",
                        {
                            email,
                            first_name,
                            last_name,
                            phone_number
                        }, {headers: {"Authorization": state.user.principal.token}});

                    if (!response.data.success) {
                        return reject(response.data.result);
                    }
                    commit(MERGE_USERS, [{id: response.data.result.id, email, first_name, last_name, phone_number}]);
                    return resolve();
                })();
            });
        },
        [GET_ALL_USERS]({commit, state}, {email, roleId, limit, page}) {
            return new Promise((resolve, reject) => {
                (async () => {
                    commit(REMOVE_USERS);
                    let query = "?";
                    let response;
                    if (roleId !== undefined) {
                        //-- get all userrole with roleId
                        //-- then pass id query to the final query
                        response = await axios.get(process.env.VUE_APP_API_SERVER_ENDPOINT + "/principalrole?role_id=" + JSON.stringify([roleId]),
                            {headers: {"Authorization": state.user.principal.token}});
                        if (!response.data.success) {
                            return reject(response.data.result);
                        }
                        if (response.data.result.length) {
                            query += "principal_id=" + JSON.stringify(response.data.result.map(pr => pr.principal_id));
                        } else {
                            return commit(MERGE_USERS, []);
                        }
                    }
                    if (email) {
                        query += "&email=" + escapeRegex(email);
                    }
                    if (limit) {
                        query += "&limit=" + limit;
                    }
                    if (page) {
                        query += "&page=" + page;
                    }
                    let users = await axios.get(process.env.VUE_APP_API_SERVER_ENDPOINT + "/user/" + query,
                        {headers: {"Authorization": state.user.principal.token}});

                    if (!users.data.success) {
                        return reject(users.data.result);
                    }

                    //--  get all principal ids then fetch for principals
                    let principalIds = users.data.result.map((user) => user.principal_id);
                    let principals = !principalIds.length ? [] : await axios.get(process.env.VUE_APP_API_SERVER_ENDPOINT + "/principal?id=" + JSON.stringify(principalIds),
                        {headers: {"Authorization": state.user.principal.token}});

                    //-- get all user roles
                    let principalRoles = !principalIds.length ? [] : await axios.get(process.env.VUE_APP_API_SERVER_ENDPOINT + "/principalrole?principal_id=" + JSON.stringify(principalIds),
                        {headers: {"Authorization": state.user.principal.token}});

                    commit(MERGE_USERS, users.data.result);
                    commit(UPDATE_PRINCIPALS, principals.data.result);
                    commit(UPDATE_PRINCIPAL_ROLES, principalRoles.data.result);

                    return resolve();
                })();
            });
        },
        [UPDATE_USER]({commit, state}, {id, principal_id, email, first_name, last_name, phone_number, user_roles}) {
            return new Promise((resolve, reject) => {
                (async () => {
                    let response = await axios.put(process.env.VUE_APP_API_SERVER_ENDPOINT + "/user/" + id,
                        {
                            email,
                            first_name,
                            last_name,
                            phone_number
                        }, {headers: {"Authorization": state.user.principal.token}});

                    if (!response.data.success) {
                        return reject(response.data.result);
                    }

                    if (user_roles) {
                        let response = await axios.put(process.env.VUE_APP_API_SERVER_ENDPOINT + "/principal/" + principal_id + "/role",
                            {
                                role_id: user_roles
                            }, {headers: {"Authorization": state.user.principal.token}});
                        if (!response.data.success) {
                            return reject(response.data.result);
                        }
                        commit(REMOVE_PRINCIPAL_ROLES, principal_id);
                        commit(UPDATE_PRINCIPAL_ROLES, response.data.result);
                    }

                    commit(MERGE_USERS, [{id, email, first_name, last_name, phone_number}]);
                    return resolve();
                })();
            });
        },
        [DELETE_USER]({commit, state}, {id}) {
            return new Promise((resolve, reject) => {
                (async () => {
                    let response = await axios.delete(process.env.VUE_APP_API_SERVER_ENDPOINT + "/user/" + id,
                        {headers: {"Authorization": state.user.principal.token}});

                    if (!response.data.success) {
                        return reject(response.data.result);
                    }
                    commit(REMOVE_USERS, [{id}]);
                    return resolve();
                })();
            });
        },
        [DISABLE_PRINCIPAL]({commit, state}, {id, disabled}) {
            return new Promise((resolve, reject) => {
                (async () => {
                    let response = await axios.put(process.env.VUE_APP_API_SERVER_ENDPOINT + "/principal/" + id,
                        {disabled}, {headers: {"Authorization": state.user.principal.token}});

                    if (!response.data.success) {
                        return reject(response.data.result);
                    }
                    commit(DISABLE_PRINCIPAL, {id, disabled});
                    return resolve();
                })();
            });
        },

        //-- policies
        [CREATE_POLICY]({commit, state}, {policy_name}) {
            return new Promise((resolve, reject) => {
                (async () => {
                    let response = await axios.post(process.env.VUE_APP_API_SERVER_ENDPOINT + "/policy",
                        {
                            policy_name,
                        }, {headers: {"Authorization": state.user.principal.token}});

                    if (!response.data.success) {
                        return reject(response.data.result);
                    }
                    commit(MERGE_POLICIES, [{id: response.data.result.id, policy_name}]);
                    return resolve(response.data.result.id);
                })();
            });
        },
        [GET_ALL_POLICIES]({commit, state}) {
            return new Promise((resolve, reject) => {
                (async () => {
                    let policies = await axios.get(process.env.VUE_APP_API_SERVER_ENDPOINT + "/policy",
                        {headers: {"Authorization": state.user.principal.token}});
                    if (!policies.data.success) {
                        return reject(policies.data.result);
                    }

                    let conditions = await axios.get(process.env.VUE_APP_API_SERVER_ENDPOINT + "/policycondition",
                        {headers: {"Authorization": state.user.principal.token}});
                    if (!conditions.data.success) {
                        return reject(conditions.data.result);
                    }

                    let permissions = await axios.get(process.env.VUE_APP_API_SERVER_ENDPOINT + "/policypermission",
                        {headers: {"Authorization": state.user.principal.token}});
                    if (!permissions.data.success) {
                        return reject(permissions.data.result);
                    }

                    let allPermissions = await axios.get(process.env.VUE_APP_API_SERVER_ENDPOINT + "/permission",
                        {headers: {"Authorization": state.user.principal.token}});
                    if (!allPermissions.data.success) {
                        return reject(allPermissions.data.result);
                    }

                    commit(MERGE_POLICIES, policies.data.result);
                    commit(MERGE_POLICY_PERMISSIONS, permissions.data.result);
                    commit(UPDATE_POLICY_CONDITIONS, conditions.data.result);
                    commit(SET_ALL_PERMISSIONS, allPermissions.data.result);
                    return resolve();

                })();
            });
        },
        [UPDATE_POLICY]({commit, state}, {id, policy_name, permissions, conditions}) {
            return new Promise((resolve, reject) => {
                (async () => {
                    let policyResult = await axios.put(process.env.VUE_APP_API_SERVER_ENDPOINT + "/policy/" + id,
                        {
                            policy_name,
                        }, {headers: {"Authorization": state.user.principal.token}});

                    if (!policyResult.data.success) {
                        return reject(policyResult.data.result);
                    }

                    let permissionResult = await axios.put(process.env.VUE_APP_API_SERVER_ENDPOINT + "/policy/" + id + "/permission",
                        {
                            permissions
                        }, {headers: {"Authorization": state.user.principal.token}});

                    if (!permissionResult.data.success) {
                        return reject(permissionResult.data.result);
                    }

                    let conditionResult = await axios.put(process.env.VUE_APP_API_SERVER_ENDPOINT + "/policy/" + id + "/condition",
                        {
                            conditions
                        }, {headers: {"Authorization": state.user.principal.token}});

                    if (!conditionResult.data.success) {
                        return reject(conditionResult.data.result);
                    }

                    commit(MERGE_POLICIES, [{id, policy_name}]);
                    commit(REMOVE_POLICY_PERMISSIONS, id);
                    commit(MERGE_POLICY_PERMISSIONS, permissionResult.data.result);
                    commit(REMOVE_POLICY_CONDITIONS, id);
                    commit(MERGE_POLICY_CONDITIONS, conditionResult.data.result);
                    return resolve();
                })();
            });
        },
        [DELETE_POLICY]({commit, state}, {id}) {
            return new Promise((resolve, reject) => {
                (async () => {
                    let response = await axios.delete(process.env.VUE_APP_API_SERVER_ENDPOINT + "/policy/" + id,
                        {headers: {"Authorization": state.user.principal.token}});

                    if (!response.data.success) {
                        return reject(response.data.result);
                    }
                    commit(REMOVE_POLICIES, [{id}]);
                    return resolve();
                })();
            });
        },
    },
});

export {configStore};
