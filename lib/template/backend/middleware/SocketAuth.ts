import {SocketConnection, ClaireError, AbstractSocketMiddleware} from "claire-framework";
import {Crypto} from "../utils/Crypto";
import {Errors} from "../utils/Errors";

export class SocketAuth extends AbstractSocketMiddleware {

    private readonly jwtKey: string;

    public constructor(jwtKey: string) {
        super();
        this.jwtKey = jwtKey;
    }

    public intercept() {
        return (connection: SocketConnection, next: (err?: any) => void) => {
            const token = connection.socket.handshake.query.token;
            Crypto.parseJsonToken(this.jwtKey, token)
                .then((result) => {
                    connection.socket.id = result.userId;
                    next();
                })
                .catch(() => {
                    next(new ClaireError(Errors.TOKEN_EXPIRED));
                });
        };
    }

}
