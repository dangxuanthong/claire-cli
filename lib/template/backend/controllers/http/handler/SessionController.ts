import {
    AbstractHttpController,
    ClaireError,
    eq,
    IAppContext,
    IDatabaseAdapter,
    IHttpRequest,
    Mapping,
    OpenAccess,
    Permission,
    Validator,
} from "claire-framework";
import {SessionService} from "../../../services/SessionService";
import {Auth, AuthType} from "../../../models/Auth";
import {User} from "../../../models/User";
import {Errors} from "../../../utils/Errors";
import {Crypto} from "../../../utils/Crypto";
import {EnvSheet} from "../../../env/EnvSheet";
import {Principal} from "../../../models/Principal";
import {CreateSession, SESSION_MANAGEMENT} from "../api/SesssionManagement";

export class SessionController extends AbstractHttpController {

    private sessionService: SessionService;
    private databaseAdapter: IDatabaseAdapter;
    private envProvider: EnvSheet;

    public constructor() {
        super();
    }

    public async init(appContext: IAppContext): Promise<void> {
        this.sessionService = appContext.getServiceProvider().get(SessionService);
        this.databaseAdapter = appContext.getDatabaseAdapter();
        this.envProvider = appContext.getEnvProvider().load(EnvSheet);
    }

    @OpenAccess()
    @Validator(CreateSession.request, CreateSession.response)
    @Permission(SESSION_MANAGEMENT, CreateSession.permissionType, CreateSession.permissionConditions)
    @Mapping(CreateSession.method, CreateSession.url)
    public async createSession(request: IHttpRequest) {
        let body = request.getBody(CreateSession.request);
        switch (body.type) {
            case AuthType.PASSWORD:
                //-- check password
                let user = await this.databaseAdapter.use(User).getOne([{[User.email]: eq(body.authData1)}]);
                if (!user) {
                    throw new ClaireError(Errors.USER_NOT_FOUND);
                }

                let auth = await this.databaseAdapter.use(Auth).getOne([{[Auth.principal_id]: eq(user.principal_id)}, {[Auth.auth_type]: eq(AuthType.PASSWORD)}]);
                if (!auth) {
                    throw new ClaireError(Errors.AUTH_NOT_FOUND);
                }

                if (Crypto.hashPassword(body.authData2) !== auth.auth_data1) {
                    throw new ClaireError(Errors.INCORRECT_PASSWORD);
                }

                let principal = await this.databaseAdapter.use(Principal).getOne([{[Principal.id]: eq(user.principal_id)}]);
                if (!principal) {
                    throw new ClaireError(Errors.USER_NOT_FOUND)
                }

                if (principal.disabled) {
                    throw new ClaireError(Errors.USER_IS_DISABLED);
                }

                return this.sessionService.createSession(principal, this.envProvider.JWT_DURATION);
            case AuthType.API_KEY:
                throw new ClaireError(Errors.NOT_SUPPORTED);
        }
    }

}
