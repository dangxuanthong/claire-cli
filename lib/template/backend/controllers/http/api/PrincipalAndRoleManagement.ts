import {HTTP, InSet, PermissionType, Required, Validate} from "claire-framework";
import {AuthType} from "../../../models/Auth";
import {Misc} from "../../../utils/Misc";

export const PRINCIPAL_AND_ROLE_MANAGEMENT = "PrincipalAndRoleManagement";

export namespace CreateSession {

    export const permissionType = PermissionType.WRITE;
    export const permissionConditions = [];

    export const method = HTTP.POST;
    export const url = "/session";

    @Validate()
    export class request {

        @InSet(Misc.getEnumValues(AuthType))
        @Required()
        public type: AuthType;

        @Required()
        public authData1: string;

        @Required()
        public authData2: string;
    }
}
