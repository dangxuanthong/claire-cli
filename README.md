## claire-cli

Generate a `claire-framework` template project. Please read `claire-framework` documentation for more info.

To install: `sudo npm i -g claire-cli`
To display current version of claire-cli: `claire-cli -v`
To run: `claire-cli <name-of-the-project>`
To start the project locally for development: `npm start` (remember to review the environment file and install dependencies before, you can use either `npm` or `yarn`).

#### Directory structure:
- name-of-the-project/
  - src/: project source
    - env/: contains definition of environment variables.
    - models/: contains models.
    - models/: contains models.
    - services/: contains implementations of services.
    - controllers/: contains the definition of controllers.
    - middleware/: contains middleware.
    - jobs/: contains jobs to be run separately but using the same application logic.
    - log/: directory to hold log files, can be change in `app.ts`.
      - example_job.ts: this is an example of job, which run commands over logic defined in `app.ts`. 
    - utils/: contains utilities functions.
    - app.ts: this file defines the core logic of the application.
    - index.ts: this file is the main entry of the application, containing definition of controllers to handle HTTP request.
    - export.ts: this file exports the HTTP Rest API of all controllers (explicitly). Run `npm run export` to obtain the `d.ts` definition to be used in the client side.
    - tsconfig.json: compilation instruction for typescript to build the project. 
  - dst/: project built version
  - export-x.y.z.d.ts: the exported version of Rest API post-fixed by the version of the current project. 
  
  
  
## Change log

1.2.4
- update to claire-framework@2.1.3

1.2.3
- refactor UserService

1.2.1
- remove old document file
- exclude node_modules

1.2.0
- Upgrade to claire-framework 2.1.0

1.1.0
- Upgrade to claire-framework@1.8.1


