import {IAppContext} from "claire-framework";
import {claireBuilder} from "../app";

//-- run this as a separated process
claireBuilder.setHttpRequestHandler(undefined);
claireBuilder.setSocketRequestHandler(undefined);

claireBuilder.build().start()
    .then((appContext: IAppContext) => {
        appContext.getLogger().info("task completed");
        process.exit();
    })
    .catch((err) => {
        console.log(err);
        process.exit();
    });
