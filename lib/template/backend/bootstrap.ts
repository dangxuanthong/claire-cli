import {eq, IAppContext, ILogger, Initable, IDatabaseAdapter, HttpRouteHandler, ClaireError} from "claire-framework";
import {Role} from "./models/Role";
import {PrincipalRole} from "./models/PrincipalRole";
import {EnvSheet} from "./env/EnvSheet";
import {Policy} from "./models/Policy";
import {RolePolicy} from "./models/RolePolicy";
import {PolicyPermission} from "./models/PolicyPermission";
import {Errors} from "./utils/Errors";
import {UserService} from "./services/UserService";

export class Bootstrap implements Initable {

    private logger: ILogger;
    private envProvider: EnvSheet;
    private databaseAdapter: IDatabaseAdapter;
    private httpEndpoints: HttpRouteHandler[];
    private userService: UserService;

    public constructor() {

    }

    public async init(appContext: IAppContext): Promise<void> {

        this.logger = appContext.getLogger();

        this.logger.info("bootstrapping...");

        this.logger = appContext.getLogger();
        this.envProvider = appContext.getEnvProvider().load(EnvSheet);
        this.databaseAdapter = appContext.getDatabaseAdapter();
        this.httpEndpoints = appContext.getHttpEndpoints();
        this.userService = appContext.getServiceProvider().get(UserService);

        this.logger.debug("initing super users");
        await this.initSuperUser();
        this.logger.info("Bootstrap completed");
    }

    public async stop(): Promise<void> {
        return;
    }

    private async initSuperUser() {
        //-- create default super admin role if not existed
        let superAdminRole = await this.databaseAdapter.use(Role).getOne([
            {[Role.is_read_only]: eq(true)},
            {[Role.is_system_role]: eq(false)}
        ]);

        if (!superAdminRole) {
            this.logger.debug("super admin role not exists, creating one");
            superAdminRole = new Role();
            superAdminRole.is_system_role = false;
            superAdminRole.is_read_only = true;
            superAdminRole.role_name = "Super Admin";
            superAdminRole = await this.databaseAdapter.use(Role).saveOne(superAdminRole);

            //-- create default full access policy
            let fullAccessPolicy = new Policy();
            fullAccessPolicy.policy_name = "Full Access";
            fullAccessPolicy = await this.databaseAdapter.use(Policy).saveOne(fullAccessPolicy);

            //-- grant this policy full access
            let handlers = this.httpEndpoints;
            let permissions: PolicyPermission[] = handlers.map((handler) => {
                let permission = new PolicyPermission();
                permission.policy_id = fullAccessPolicy.id;
                permission.permission = handler.method + handler.url;
                return permission;
            });

            await this.databaseAdapter.use(PolicyPermission).saveMany(permissions);

            //-- attach policy to role
            let rolePolicy = new RolePolicy();
            rolePolicy.role_id = superAdminRole.id;
            rolePolicy.policy_id = fullAccessPolicy.id;
            await this.databaseAdapter.use(RolePolicy).saveOne(rolePolicy);

            let superAdmin = await this.userService.createNewUser(this.envProvider.ADMIN_EMAIL!, this.envProvider.INITIAL_ADMIN_PASSWORD!);

            //-- attach role
            let identityRole = new PrincipalRole();
            identityRole.principal_id = superAdmin.principal.id;
            identityRole.role_id = superAdminRole.id;
            await this.databaseAdapter.use(PrincipalRole).saveOne(identityRole);

            this.logger.verbose("admin user created");
        } else {

            this.logger.debug("getting policy");
            //-- get super admin full access policy and grant all access
            let policy = await this.databaseAdapter.use(RolePolicy).getOne([{[RolePolicy.role_id]: eq(superAdminRole.id)}]);
            if (!policy) {
                throw new ClaireError(Errors.USER_NOT_FOUND, "granting permission for existing super admin");
            }

            this.logger.debug("removing old permissions");
            //-- remove old permissions
            await this.databaseAdapter.use(PolicyPermission).deleteMany([{[PolicyPermission.policy_id]: eq(policy.id)}]);

            //-- grant this policy full access
            this.logger.debug("grating all permissions");
            let handlers = this.httpEndpoints;
            // console.log("all handlers", handlers);
            let permissions: PolicyPermission[] = handlers.map((handler) => {
                let permission = new PolicyPermission();
                permission.policy_id = policy!.id;
                permission.permission = `${handler.method}${handler.url}`;
                return permission;
            });

            // console.log("permissions", permissions);

            this.logger.debug("saving...");
            await this.databaseAdapter.use(PolicyPermission).saveMany(permissions);

            this.logger.debug("done");
        }

    }

}
