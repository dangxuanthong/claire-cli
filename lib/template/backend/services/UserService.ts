import {AbstractService, IAppContext, IDatabaseAdapter} from "claire-framework";
import {Principal} from "../models/Principal";
import {User} from "../models/User";
import {Auth, AuthType} from "../models/Auth";
import {Crypto} from "../utils/Crypto";

export class UserService extends AbstractService {

    private databaseAdapter: IDatabaseAdapter;

    public constructor() {
        super()
    }

    public async init(appContext: IAppContext) {
        this.databaseAdapter = appContext.getDatabaseAdapter();
    }

    public async createNewUser(email: string, password: string, first_name?: string, last_name?: string, phone_number?: string): Promise<{ principal: Principal, user: User, auth: Auth }> {

        let principal = new Principal();
        principal = await this.databaseAdapter.use(Principal).saveOne(principal);

        let user = new User();
        user.first_name = first_name;
        user.last_name = last_name;
        user.phone_number = phone_number;
        user.email = email;
        user.principal_id = principal.id;

        user = await this.databaseAdapter.use(User).saveOne(user);

        //-- create auth for this user
        let auth = new Auth();
        auth.principal_id = principal.id;
        auth.auth_type = AuthType.PASSWORD;
        auth.auth_data1 = Crypto.hashPassword(password);
        await this.databaseAdapter.use(Auth).saveOne(auth);

        return {principal, user, auth};
    }

}
