import {Charset, DataTable, Default, PrimaryKey, Unique} from "claire-framework";

@DataTable(Charset.UTF8)
export class Role {

    @PrimaryKey(true)
    public id: number;

    @Unique()
    public role_name: string;

    @Default(false)
    public is_read_only: boolean;

    @Default(false)
    public is_system_role: boolean;

    public static id = "id";
    public static role_name = "role_name";
    public static is_read_only = "is_read_only";
    public static is_system_role = "is_system_role";

}
