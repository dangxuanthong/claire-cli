import {Charset, DataTable, ForeignKey, PrimaryKey} from "claire-framework";
import {Role} from "./Role";
import {Policy} from "./Policy";

@DataTable(Charset.UTF8)
export class RolePolicy {

    @PrimaryKey(true)
    public id: number;

    @ForeignKey(Role)
    public role_id: number;

    @ForeignKey(Policy)
    public policy_id: number;

    public static id = "id";
    public static role_id = "role_id";
    public static policy_id = "policy_id";

}
