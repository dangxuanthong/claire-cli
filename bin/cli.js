#!/usr/bin/env node

var version = require("../package").version;
//--  delete the 0 and 1 argument (node and script.js)
var args = process.argv.splice(process.execArgv.length + 2);

//-- retrieve the first argument
var projectName = args[0];

//-- copy the lib directory to current directory and rename
if (!projectName) {
    console.log("Please supply a project name without space.");
    process.exit(1);
} else {
    if (["-v", "--version"].indexOf(projectName) >= 0) {
        console.log(version);
    } else {
        var ncp = require('ncp').ncp;
        var path = require("path");

        var destinationPath = path.join(process.cwd(), projectName);
        ncp(path.join(__dirname, "../lib/template"), destinationPath, function (err) {
            if (err) {
                return console.error(err);
            }
            console.log('Done!');
        });
    }

}

