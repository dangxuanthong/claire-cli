export const LOG_IN = "LOG_IN";
export const LOG_OUT = "LOG_OUT";

//-- roles
export const CREATE_ROLE = "CREATE_ROLE";
export const GET_ALL_ROLES = "GET_ALL_ROLES";
export const UPDATE_ROLE = "UPDATE_ROLE";
export const DELETE_ROLE = "DELETE_ROLE";

//-- users
export const CREATE_USER = "CREATE_USER";
export const GET_ALL_USERS = "GET_ALL_USERS";
export const UPDATE_USER = "UPDATE_USER";
export const DELETE_USER = "DELETE_USER";
export const DISABLE_PRINCIPAL = "DISABLE_PRINCIPAL";

//-- policies
export const CREATE_POLICY = "CREATE_POLICY";
export const GET_ALL_POLICIES = "GET_ALL_POLICIES";
export const UPDATE_POLICY = "UPDATE_POLICY";
export const DELETE_POLICY = "DELETE_POLICY";

