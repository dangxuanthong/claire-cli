import jwt from "jsonwebtoken";
import crypto from "crypto";

export class Crypto {
    public static hashPassword(password: string): string {
        let hash = crypto.createHash("SHA256");
        hash.update(password);
        return hash.digest().toString("base64");
    }

    public static async generateJsonToken(secret: string, data: any, expiration: string): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            jwt.sign(data, secret, {expiresIn: expiration}, (err: any, encoded: string) => {
                if (err) {
                    return reject(err);
                }
                return resolve(encoded);
            });
        })
    }

    public static async parseJsonToken(secret: string, encoded: string): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            jwt.verify(encoded, secret, (err, decoded) => {
                if (err) {
                    return reject(err);
                }
                return resolve(decoded);
            });
        });
    }
}