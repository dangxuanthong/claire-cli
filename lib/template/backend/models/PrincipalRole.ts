import {Charset, DataTable, ForeignKey, PrimaryKey} from "claire-framework";
import {Role} from "./Role";
import {Principal} from "./Principal";

@DataTable(Charset.UTF8)
export class PrincipalRole {

    @PrimaryKey(true)
    public id: number;

    @ForeignKey(Principal)
    public principal_id: number;

    @ForeignKey(Role)
    public role_id: number;

    public static id = "id";
    public static principal_id = "principal_id";
    public static role_id = "role_id";

}
