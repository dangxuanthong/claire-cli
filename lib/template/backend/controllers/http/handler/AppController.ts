import {
    DefaultHttpResourceController,
    IAppContext,
    IDatabaseAdapter,
    IHttpRequest,
    Mapping, Override,
    Permission,
    Validator
} from "claire-framework";
import {App} from "../../../models/App";
import {CreateNewApp, USER_AND_APP_MANAGEMENT} from "../api/UserAndAppManagement";
import {Principal} from "../../../models/Principal";

export class AppController extends DefaultHttpResourceController<App> {

    private databaseAdapter: IDatabaseAdapter;

    public constructor(model: typeof App) {
        super(model);
    }

    public async init(appContext: IAppContext) {
        await super.init(appContext);
        this.databaseAdapter = appContext.getDatabaseAdapter();
    }

    @Override()
    @Validator(CreateNewApp.request, CreateNewApp.response)
    @Permission(USER_AND_APP_MANAGEMENT, CreateNewApp.permissionType, CreateNewApp.permissionConditions)
    @Mapping(CreateNewApp.method, CreateNewApp.url)
    public async createNewApp(request: IHttpRequest) {

        let body = request.getBody(CreateNewApp.request);

        //-- create new principal for this app
        let principal = new Principal();
        principal = await this.databaseAdapter.use(Principal).saveOne(principal);

        let app = new App();
        app.principal_id = principal.id;
        app.app_name = body.app_name;

        app = await this.databaseAdapter.use(App).saveOne(app);

        return {id: app.id};
    }

}
