import Vue from 'vue';

Vue.config.productionTip = false;

import {i18n} from "./config/locale";
import {configStore} from "./store/store";
import {configRouter} from "./config/routing";
import "./config/lib";
import "./style.scss";

import App from "./App";

const store = configStore();

//-- create root instance
new Vue({
    store,
    i18n,
    router: configRouter(store),
    render: (h) => h(App),
}).$mount('#app');
