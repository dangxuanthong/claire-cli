//-- routing
import Vue from "vue";
import VueRouter from 'vue-router';
import LoginPage from "../pages/login/LoginPage";
import MainPage from "../pages/home/MainPage";
import WelcomeScreen from "../pages/home/welcome/WelcomeScreen";
import UserScreen from "../pages/home/user-screen/UserScreen";
import CreateUserScreen from "../pages/home/user-screen/CreateUserScreen";
import RoleManagement from "../pages/home/role-screen/RoleManagement";
import PolicyPermissions from "../pages/home/role-screen/PolicyPermissions";

Vue.use(VueRouter);

const configRouter = (store) => new VueRouter({
    mode: "history",
    routes: [
        {
            path: "/",
            component: LoginPage
        },
        {
            path: "/home",
            component: MainPage,
            beforeEnter: (to, from, next) => {
                if (!store.state.user) {
                    return next(false);
                }
                return next();
            },
            children: [
                {path: "", component: WelcomeScreen},
                {path: "users", component: UserScreen},
                {path: "users/new", component: CreateUserScreen},
                {path: "roles", component: RoleManagement},
                {path: "policies", component: PolicyPermissions},
            ]
        },
    ]
});

export {configRouter};
