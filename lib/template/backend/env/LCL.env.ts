import {LogLimit} from "claire-framework";
import {EnvSheet} from "./EnvSheet";

export const env: EnvSheet = {
    PORT: 1992,
    LOG_LIMIT: LogLimit.DEBUG,
    JWT_DURATION: "24h",

    JWT_KEY: "930570tc8w978097x0tw7e097w9085729387498wyer9",
    SQL_CONNECTION_STRING: "root:vinamilk@127.0.0.1:3306/claire_base",
    MONGO_CONNECTION_STRING: "127.0.0.1:27017/claire_base",
    ADMIN_EMAIL: "admin@localhost",
    INITIAL_ADMIN_PASSWORD: "vinamilk",
};
