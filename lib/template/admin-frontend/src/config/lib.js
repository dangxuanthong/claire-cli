//-- perfect scrollbar
import Vue from "vue";
import PerfectScrollbar from 'vue2-perfect-scrollbar';
import 'vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css';

Vue.use(PerfectScrollbar);

//-- register ant-design component
import {
    Form, Input, Button, Checkbox, Icon, Menu, Avatar,
    Select, Collapse, Pagination, Popconfirm, Switch, Card,
    Tabs, Modal, List, Tag,
    notification
} from "ant-design-vue";

Vue.use(Form);
Vue.use(Input);
Vue.use(Button);
Vue.use(Checkbox);
Vue.use(Menu);
Vue.use(Icon);
Vue.use(Avatar);
Vue.use(Select);
Vue.use(Collapse);
Vue.use(Pagination);
Vue.use(Popconfirm);
Vue.use(Switch);
Vue.use(Card);
Vue.use(Tabs);
Vue.use(Modal);
Vue.use(List);
Vue.use(Tag);
Vue.prototype.$notification = notification;
