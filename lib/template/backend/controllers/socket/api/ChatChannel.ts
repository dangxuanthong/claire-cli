import {Required, Validate} from "claire-framework";

export namespace ChatChannel {
    export namespace Join {

        export const channel = "/join";

        @Validate()
        export class message {
            @Required()
            room_name: string;
        }
    }
}
