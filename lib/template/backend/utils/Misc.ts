export class Misc {

    public static getEnumValues(enumObject: any): any[] {
        let values = Object.values(enumObject);
        let typeDiff = false;
        for (let i = 0; i < values.length - 1; i++) {
            if (typeof values[i] !== typeof values[i + 1]) {
                typeDiff = true;
                break;
            }
        }
        return typeDiff ? values.slice(values.length / 2) : values;
    }

}
