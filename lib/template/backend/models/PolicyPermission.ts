import {Charset, DataField, DataTable, ForeignKey, PrimaryKey} from "claire-framework";
import {Policy} from "./Policy";

@DataTable(Charset.UTF8)
export class PolicyPermission {

    @PrimaryKey(true)
    public id: number;

    @ForeignKey(Policy)
    public policy_id: number;

    @DataField()
    public permission: string;

    public static id = "id";
    public static policy_id = "policy_id";
    public static permission = "permission";

}
