import {HTTP, Optional, PermissionType, Required, Validate} from "claire-framework";

export const USER_AND_APP_MANAGEMENT = "UserAndAppManagement";

export namespace CreateNewApp {

    export const permissionType = PermissionType.WRITE;
    export const permissionConditions = [];

    export const method = HTTP.POST;
    export const url = "/app";

    @Validate()
    export class request {

        @Required()
        public app_name: string;

    }

    @Validate()
    export class response {

        @Required()
        public id: number;

    }
}

export namespace CreateNewUser {
    export const permissionType = PermissionType.WRITE;
    export const permissionConditions = [];

    export const method = HTTP.POST;
    export const url = "/user";

    @Validate()
    export class request {

        @Required()
        public email: string;

        @Required()
        public password: string;

        @Optional()
        public first_name: string;

        @Optional()
        public last_name: string;

        @Optional()
        public phone_number: string;

    }

    @Validate()
    export class response {

        @Required()
        public id: number;

        @Required()
        public principal_id: number;

    }
}
