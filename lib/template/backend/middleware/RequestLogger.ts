import {AbstractHttpMiddleware, ILogger, HttpConnection, IAppContext} from "claire-framework";

export class RequestLogger extends AbstractHttpMiddleware {

    private logger: ILogger;

    public constructor() {
        super();
    }

    public async init(appContext: IAppContext): Promise<void> {
        this.logger = appContext.getLogger();
    }

    public intercept() {
        return (connection: HttpConnection, next: (err?: any) => void): void => {
            this.logger.info(connection.request.method, connection.request.url);
            return next();
        };
    }

}
