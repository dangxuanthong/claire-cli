import {Charset, DataTable, ForeignKey, Nullable, PrimaryKey, Unique} from "claire-framework";
import {Principal} from "./Principal";

@DataTable(Charset.UTF8)
export class User {

    @PrimaryKey(true)
    public id: number;

    @Unique()
    public email: string;

    @Nullable()
    public first_name?: string;

    @Nullable()
    public last_name?: string;

    @Nullable()
    public phone_number?: string;

    @ForeignKey(Principal)
    public principal_id: number;

    public static id = "id";
    public static email = "email";
    public static first_name = "first_name";
    public static last_name = "last_name";
    public static phone_number = "phone_number";
    public static principal_id = "principal_id;"

}
