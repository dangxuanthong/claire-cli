import {AbstractSocketAuthorizationProvider, SocketChannelHandler, SocketConnection} from "claire-framework";

export class SocketAuthorizationProvider extends AbstractSocketAuthorizationProvider {

    public constructor() {
        super();
    }

    public async authorize(connection: SocketConnection, handler: SocketChannelHandler): Promise<void> {

    }


}
