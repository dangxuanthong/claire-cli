import {Charset, DataTable, PrimaryKey, Unique} from "claire-framework";

@DataTable(Charset.UTF8)
export class Policy {

    @PrimaryKey(true)
    public id: number;

    @Unique()
    public policy_name: string;

    public static id = "id";
    public static policy_name = "policy_name";

}
